<?php
    header('Content-Type: text/html; charset=utf-8');
    //open connection to mysql db
    $connection = mysqli_connect("localhost","readonly","readonly123","happyhours_db") or die("Error " . mysqli_error($connection));
    mysqli_set_charset('utf8',$connection);
    mysqli_query($connection,"set character_set_client='utf8'"); 
    mysqli_query($connection,"set character_set_results='utf8'"); 
    //fetch table rows from mysql db
    $sql = "select * from deal_items where is_active is true";
    $result = mysqli_query($connection, $sql) or die("Error in Selecting " . mysqli_error($connection));

    //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }
    //close the db connection
    mysqli_close($connection);
    // echo results
    echo json_encode($emparray);
?>