if(window.location.href.indexOf("index_en.html") > -1) {
       //alert("english");
       is_english=true;
    }
else {
  is_english=false;
}
// Global Map View Vars
var map;
var tlv = new google.maps.LatLng(32.080942, 34.780719);
var br_markers = [];
var bl_markers = [];
var ln_markers = [];
var hh_markers = [];

// Global time date variables
function pad(val){
    stringVal = String(val);
     return (val<10) ? '0' + stringVal : stringVal;
}

var d = new Date();
var h = d.getHours();
var m = d.getMinutes();
var currentTimeString = parseInt(pad(h) + pad(m));
var currentDay = d.getDay();
// CORS HTTP function
function createCORSRequest(method, url) {
  var xhr = new XMLHttpRequest();
  if ("withCredentials" in xhr) {

    // Check if the XMLHttpRequest object has a "withCredentials" property.
    // "withCredentials" only exists on XMLHTTPRequest2 objects.
    xhr.open(method, url, true);

  } else if (typeof XDomainRequest != "undefined") {

    // Otherwise, check if XDomainRequest.
    // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
    xhr = new XDomainRequest();
    xhr.open(method, url);

  } else {

    // Otherwise, CORS is not supported by the browser.
    xhr = null;

  }
  return xhr;
}

//var baseDivString =  "<div class='element-item db_open_now db_open_sunday db_open_monday db_open_tuesday db_open_wednesday db_open_thursday db_open_friday db_open_saturday' data-random='db_random' data-category='db_category' data-id='db_object_id' data-headline='db_headline' lat='db_lat' long='db_long' data-distance='db_distance'><div class='item-main-info hover-class db_object_id' id='db_object_id' onclick='showOther(this.id)'><div class='bg_img'><div class='color-strip item-strip db_category_color'></div><img id='bg-img' data-original='images/db_image_url' src='images/grey.jpg'/><div class='item-tag-icon'><img src='assets/tag_db_category.png'/></div><div class='item-headline'><div class='item-name'>db_headline</div><div class='item-address'>db_address</div></div></div><div class='main-details'><div class='main-offer'>db_main_offer</div><div class='main-hours'>db_deal_hours <img src='assets/open_now.png' style='db_open_image'/></div><div class='main-distance'><img src='assets/location_icon.png'/> db_distance distance_string</div></div><div class='switch_button hover-class' id='db_object_id' onclick='showOther(this.id);'>details_string <img class='arrow-img' src='arrow_string'/></div></div><div class='item-other-info db_object_id'><iframe class='map-item lazy' data-src='db_google_maps_url'></iframe><div class='more-offer'><div class='offer-contacts'><div class='item-phone-number'><img src='assets/phone_icon.png'/> <a class='a-phone-number' href='tel:db_phone' target='_blank'>db_phone</a></div><div class='item-url'><img src='assets/house_icon.png'/> <a class='a-item-url' href='db_site_url' target='_blank'>website_string</a></div></div><div class='offer-details'>db_offer_details</div></div><div class='switch_button hover-class' id='db_object_id' onclick='showGeneral(this.id)'>back_string <img class='arrow-img' src='arrow_string'/></div></div><div class='sort-by-string' style='display:none'>db_headline db_main_offer db_offer_details</div></div>";
var baseDivString =  "<div class='element-item db_open_now db_open_sunday db_open_monday db_open_tuesday db_open_wednesday db_open_thursday db_open_friday db_open_saturday' data-random='db_random' data-category='db_category' data-id='db_object_id' data-headline='db_headline' lat='db_lat' long='db_long' data-distance='db_distance'><div class='item-main-info hover-class db_object_id' id='db_object_id' onclick='showOther(this.id)'><div class='bg_img'><div class='color-strip item-strip db_category_color'></div><img id='bg-img' data-original='images/db_image_url' src='images/grey.jpg'/><div class='item-tag-icon'><img src='assets/tag_db_category.png'/></div><div class='item-headline'><div class='item-name'>db_headline</div><div class='item-address'>db_address</div></div></div><div class='main-details'><div class='main-offer'>db_main_offer</div><div class='main-hours'>db_deal_hours <img src='assets/open_now.png' style='db_open_image'/></div><div class='main-distance'><img src='assets/location_icon.png'/> db_distance distance_string</div></div><div class='switch_button hover-class' id='db_object_id' onclick='showOther(this.id);'>details_string <img class='arrow-img' src='arrow_string'/></div></div><div class='item-other-info db_object_id'><iframe class='map-item lazy' data-src='db_google_maps_url'></iframe><div class='more-offer'><div class='offer-contacts'><div class='item-phone-number'><img src='assets/phone_icon.png'/> <a class='a-phone-number' href='tel:db_phone' target='_blank'>db_phone</a></div><div class='item-url'><img src='assets/house_icon.png'/> <a class='a-item-url' href='redirector.html?url=db_site_url'>website_string</a></div></div><div class='offer-details'>db_offer_details</div></div><div class='switch_button hover-class' id='db_object_id' onclick='showGeneral(this.id)'>back_string <img class='arrow-img' src='arrow_string'/></div></div><div class='sort-by-string' style='display:none'>db_headline db_main_offer db_offer_details</div></div>";

//var baseDivStringMap =  "<div class='element-item-map' data-random='db_random' data-category='db_category' data-id='db_object_id' data-headline='db_headline' lat='db_lat' long='db_long' data-distance='db_distance'><div class='bg_img_map'><div class='item-tag-icon item-tag-icon-map'><img src='assets/tag_db_category.png'/></div><div class='item-headline item-headline-map'><div class='item-name'>db_headline</div><div class='item-address'>db_address</div></div></div><div class='main-details'><div class='main-offer'>db_main_offer</div><div class='main-hours'>db_deal_hours</div><div class='main-distance'><img src='assets/location_icon.png'/> db_distance distance_string</div></div><div class='more-offer'><div class='offer-contacts offer-contacts-map'><div class='item-phone-number'><img src='assets/phone_icon.png'/> <a class='a-phone-number' href='tel:db_phone' target='_blank'>db_phone</a></div><div class='item-url'><img src='assets/house_icon.png'/> <a class='a-item-url' href='db_site_url' target='_blank'>website_string</a></div></div></div></div>";
var baseDivStringMap =  "<div class='element-item-map' data-random='db_random' data-category='db_category' data-id='db_object_id' data-headline='db_headline' lat='db_lat' long='db_long' data-distance='db_distance'><div class='bg_img_map'><div class='color-strip item-strip item-strip-map db_category_color'></div><div class='item-headline item-headline-map'><div class='item-name'>db_headline</div><div class='item-address'>db_address</div></div></div><div class='main-details'><div class='main-offer'>db_main_offer</div><div class='main-hours'>db_deal_hours <img src='assets/open_now.png' style='db_open_image'/></div><div class='main-distance'><img src='assets/location_icon.png'/> db_distance distance_string</div></div><div class='more-offer'><div class='offer-contacts offer-contacts-map'><div class='item-phone-number'><img src='assets/phone_icon.png'/> <a class='a-phone-number' href='tel:db_phone' target='_blank'>db_phone</a></div><div class='item-url'><img src='assets/house_icon.png'/> <a class='a-item-url' href='db_site_url' target='_blank'>website_string</a></div></div></div></div>";

if (is_english) {
  // Set baseDivString constants to english
  baseDivString = baseDivString.replace(/distance_string/g, "Km Away");
  baseDivString = baseDivString.replace(/back_string/g, "Back");
  baseDivString = baseDivString.replace(/details_string/g, "Click For Details");
  baseDivString = baseDivString.replace(/website_string/g,"Website");
  baseDivString = baseDivString.replace(/arrow_string/g,"assets/main_arrow_icon_en.png");

  baseDivStringMap = baseDivStringMap.replace(/distance_string/g, "Km Away");
  baseDivStringMap = baseDivStringMap.replace(/back_string/g, "Back");
  baseDivStringMap = baseDivStringMap.replace(/details_string/g, "Click For Details");
  baseDivStringMap = baseDivStringMap.replace(/website_string/g,"Website");
  baseDivStringMap = baseDivStringMap.replace(/arrow_string/g,"assets/main_arrow_icon_en.png");
}
else {
  // set baseDivString constants to hebrew
  baseDivString = baseDivString.replace(/distance_string/g, "ק''מ ממיקומך");
  baseDivString = baseDivString.replace(/back_string/g, "חזרה");
  baseDivString = baseDivString.replace(/details_string/g, "לחץ לפרטים נוספים");
  baseDivString = baseDivString.replace(/website_string/g,"אתר העסק");
  baseDivString = baseDivString.replace(/arrow_string/g,"assets/main_arrow_icon.png");

  baseDivStringMap = baseDivStringMap.replace(/distance_string/g, "ק''מ ממיקומך");
  baseDivStringMap = baseDivStringMap.replace(/back_string/g, "חזרה");
  baseDivStringMap = baseDivStringMap.replace(/details_string/g, "לחץ לפרטים נוספים");
  baseDivStringMap = baseDivStringMap.replace(/website_string/g,"אתר העסק");
  baseDivStringMap = baseDivStringMap.replace(/arrow_string/g,"assets/main_arrow_icon.png");

}
$(window).scroll( function() {
    var value = $(this).scrollTop();
    if ( value > 250 ) {
        $(".site-header-navigation-bar-sticky").removeClass('fadeOutUp');
        $(".site-header-navigation-bar-sticky").addClass('animated fadeInDown');
        $(".site-header-navigation-bar-sticky").css("display", "block");
      }
    else{
        $(".site-header-navigation-bar-sticky").removeClass('fadeInDown');
        $(".site-header-navigation-bar-sticky").addClass('animated fadeOutUp');
        //$(".site-header-navigation-bar").css("display", "none");
        //$(".site-header-navigation-bar").addClass('animated fadeOutUp');
      }
});

function openLightbox(boxId) {
    document.getElementById(boxId).style.display='block';
    google.maps.event.trigger(map, 'resize');
    map.setCenter(tlv);
    document.getElementById('fade').style.display='block';
    /* try closing mobile menu */
    /*$(".dl-menu").removeClass("dl-menuopen");
    $(".dl-menu").addClass( 'dl-menu-toggle' );
    $(".dl-trigger").removeClass("dl-active"); */
    $( "#dl-menu-btn" ).trigger( "click" );
  };

function closeLightbox(boxId) {
    document.getElementById(boxId).style.display='none';
    document.getElementById('fade').style.display='none';
  };

function updateFilterSelection(data_filter,isMobile) {
  // Remove no results message
 $("#no_filter_results").css("display","none");
 var $grid = $('.grid');
 // Remove animations so they don't appear post the sort/filter
 $(".item-main-info").removeClass("animated flipInY");
 $(".item-other-info").removeClass("animated flipInY");
 // Get data_filter value
 if (data_filter != "none") {
   currentFilterCategory = data_filter
 }
 /*
 if (isMobile==true) {
  // Get search string value from mobile
  currentFilterString = $('.search-box-text-mobile').val();
 }
 else {
  // Get search box string value for desktop
 currentFilterString = $('.search-box-text').val();
 }*/

 if ($('.search-box-text-mobile').val() != "") {
  currentFilterString = $('.search-box-text-mobile').val();
 }
 else {
  currentFilterString = $('.search-box-text').val();
 }
 currentFilterString = currentFilterString.toLowerCase();
 // Get Selected Time Filter
 currentFilterTime = $('#filter-menu').val();

 // Initiate updated filter
 $grid.isotope({ filter: function() {
 var itemCategory = $(this).attr("data-category");
  if (itemCategory.indexOf(currentFilterCategory) > -1) {
    // check if search string exists
    var jointString = $(this).find('.sort-by-string').text().toLowerCase();
    if (jointString.indexOf(currentFilterString) > -1) {
      // check time filter is ok
      if (currentFilterTime == null || currentFilterTime == "all") {
        return true;
      }
      else  {
        return ($(this).hasClass(currentFilterTime));
      }
    }
  }
  else {
    return false;
  }
  }});
 // Check if filter is empty - show no results div
 if ($grid.data('isotope').filteredItems.length==0) {
  $("#no_filter_results").css("display","block");
 }
};

function initIsotope() {
  if (is_english) {
    is_origin_left=true;
  }
  else {
    is_origin_left=false;
  }
  var $grid = $('.grid')
  .isotope({
      // options
      // for RTL support
      isOriginLeft: is_origin_left,
      itemSelector: '.element-item',
      masonry: {
        columnWidth: 300,
        gutter: 22,
        isFitWidth: true
      }, 
      //transitionDuration: 0,
      getSortData: {
        dataCategory: '[data-category]',
        distance: '[data-distance] parseFloat',
        headline: '.item-name',
        realRandom: '[data-random] parseFloat'
      },
      sortBy : 'realRandom'
    });

    //$grid.isotope( 'on', 'layoutComplete', function() { alert('layout complete');} );
    // Filter items on main category click

    /*$('.site-header-button').on( 'click',function() {
      $(".item-main-info").removeClass("animated flipInY");
      $(".item-other-info").removeClass("animated flipInY");
      var filterValue = $(this).attr('data-filter');
      $grid.isotope({ filter: filterValue });
    });*/
    /*
    // Update category filter buttons - Desktop
    $('.site-header-button').on('click',function() {
     //$(this).toggleClass('site-header-click');
     updateFilterSelection($(this).attr('data-filter'),false);
     // Update background of chosen category 
     $('.site-header-button').css("background-color","transparent");
     $(this).css("background-color", "rgba(0, 0, 0, 0.6)");
     });

    // Update category filter buttons - Mobile
    $('#mobile-category-menu').on('change', function() {
      updateFilterSelection($(this).val(),true);
    });

    $('#sort-menu').on('change', function() {
      // Remove animations so they don't appear post the sort/filter
      $(".item-main-info").removeClass("animated flipInY");
      $(".item-other-info").removeClass("animated flipInY");
      var sortByValue = $(this).val();
      $grid.isotope({ sortBy: sortByValue });
    });
  */
    // Try Image lazyload
    var $win = $(window);
    var $imgs = $("img");
       
    function loadVisible($els, trigger) {
        $els.filter(function () {
            var rect = this.getBoundingClientRect();
            return rect.top >= 0 && rect.top <= window.innerHeight;
        }).trigger(trigger);
    }

    $win.on('scroll', function () {
        loadVisible($imgs, 'lazylazy');
    });

    $imgs.lazyload({
        threshold : 200,
        effect: "fadeIn",
        failure_limit: Math.max($imgs.length - 1, 0),
        event: 'lazylazy'
    });

    $grid.isotope('on', 'arrangeComplete', function () {
        loadVisible($imgs, 'lazylazy');
    });

    // instantiate recliner
            $('.lazy').recliner({
                attrib: "data-src", // selector for attribute containing the media src
                throttle: 300,      // millisecond interval at which to process events
                threshold: 100,     // scroll distance from element before its loaded
                live: true          // auto bind lazy loading to ajax loaded elements          
            });
};

function generateDivs(deal_items) {
  function bindInfoWindow(marker, map, infowindow, description) {
    marker.addListener('click', function() {
        infowindow.setContent(description);
        infowindow.open(map, this);
    });
    };

  map = new google.maps.Map(document.getElementById('map-view'), {
      zoom: 15,
      center: tlv,
      mapTypeId:google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false
    });
  var infobubble = new InfoBubble({
    disableAutoPan: false,
    maxHeight: 400,
    maxWidth: 300
    //minWidth: 320
  });

  for (var i = 0; i < deal_items.length; i++) {
      var item = deal_items[i];
      if (item.get("is_active")) {
      //  if (true) {
        var copyDiv = baseDivString;
        var copyDivMap = baseDivStringMap;
        // Check if open now
        // set not open nowclass as default
        var opening_hours = item.get("opening_hours");
        if (opening_hours) {
          if (isOpenNow(opening_hours)) {
            // set class to filtered one
            copyDiv = copyDiv.replace(/db_open_now/g, "open_now");
            copyDivMap = copyDivMap.replace(/db_open_now/g,"open_now");
            // display image of open_now
            copyDiv = copyDiv.replace(/db_open_image/g,"display:inline");
            copyDivMap = copyDivMap.replace(/db_open_image/g,"display:inline");
          } 
          else 
          {
            // set not open nowclass
            copyDiv = copyDiv.replace(/db_open_now/g, "not_open_now");
            copyDivMap = copyDivMap.replace(/db_open_now/g, "not_open_now");
            // display image of open_now
            copyDiv = copyDiv.replace(/db_open_image/g,"display:none");
            copyDivMap = copyDivMap.replace(/db_open_image/g,"display:none");
          };
        }
        else {
          copyDiv = copyDiv.replace(/db_open_now/g, "not_open_now");
          // display image of open_now
          copyDiv = copyDiv.replace(/db_open_image/g,"display:none");
          copyDivMap = copyDivMap.replace(/db_open_image/g,"display:none");
        };

        // Check which days its open
        if (opening_hours) {
          if (opening_hours[0] != 0 && opening_hours[0].length > 0) {
            copyDiv = copyDiv.replace(/db_open_sunday/g, "open_sunday");
          }
          if (opening_hours[1] != 0 && opening_hours[1].length > 0) {
            copyDiv = copyDiv.replace(/db_open_monday/g, "open_monday");
          }
          if (opening_hours[2] != 0 && opening_hours[2].length > 0) {
            copyDiv = copyDiv.replace(/db_open_tuesday/g, "open_tuesday");
          }
          if (opening_hours[3] != 0 && opening_hours[3].length > 0) {
            copyDiv = copyDiv.replace(/db_open_wednesday/g, "open_wednesday");
          }
          if (opening_hours[4] != 0 && opening_hours[4].length > 0) {
            copyDiv = copyDiv.replace(/db_open_thursday/g, "open_thursday");
          }
          if (opening_hours[5] != 0 && opening_hours[5].length > 0) {
            copyDiv = copyDiv.replace(/db_open_friday/g, "open_friday");
          }
          if (opening_hours.length > 6 && opening_hours[6] != 0 && opening_hours[6].length > 0) {
            copyDiv = copyDiv.replace(/db_open_saturday/g, "open_saturday");
          }
        }
        // Retrieve according to language
        if (is_english) {
          copyDiv = copyDiv.replace(/db_headline/g, item.get("headline_en"));
          copyDiv = copyDiv.replace(/db_address/g, item.get("address_en"));
          copyDiv = copyDiv.replace(/db_main_offer/g, item.get("main_offer_en"));
          copyDiv = copyDiv.replace(/db_deal_hours/g, item.get("days_en"));
          var db_details = item.get("offer_details_en");

          copyDivMap = copyDivMap.replace(/db_headline/g, item.get("headline_en"));
          copyDivMap = copyDivMap.replace(/db_address/g, item.get("address_en"));
          copyDivMap = copyDivMap.replace(/db_main_offer/g, item.get("main_offer_en"));
          copyDivMap = copyDivMap.replace(/db_deal_hours/g, item.get("days_en")); 

        }
        else {
          copyDiv = copyDiv.replace(/db_headline/g, item.get("headline"));
          copyDiv = copyDiv.replace(/db_address/g, item.get("address"));
          copyDiv = copyDiv.replace(/db_main_offer/g, item.get("main_offer"));
          copyDiv = copyDiv.replace(/db_deal_hours/g, item.get("days"));
          var db_details = item.get("offer_details");

          copyDivMap = copyDivMap.replace(/db_headline/g, item.get("headline"));
          copyDivMap = copyDivMap.replace(/db_address/g, item.get("address"));
          copyDivMap = copyDivMap.replace(/db_main_offer/g, item.get("main_offer"));
          copyDivMap = copyDivMap.replace(/db_deal_hours/g, item.get("days"));
        }

        // Items that are the same for every language
        copyDiv = copyDiv.replace(/db_image_url/g, item.get("image_url"));
        copyDiv = copyDiv.replace(/db_icon_image_url/g, item.get("image_url"));
        copyDiv = copyDiv.replace(/db_phone/g, item.get("phone"));
        copyDiv = copyDiv.replace(/db_site_url/g, item.get("link"));
        copyDiv = copyDiv.replace(/db_lat/g, item.get("lat"));
        copyDiv = copyDiv.replace(/db_long/g, item.get("lon"));
        copyDiv = copyDiv.replace(/db_category/g, item.get("category"));
        copyDiv = copyDiv.replace(/db_object_id/g, item.get("object_id"));
        
        copyDivMap = copyDivMap.replace(/db_image_url/g, item.get("image_url"));
        copyDivMap = copyDivMap.replace(/db_icon_image_url/g, item.get("image_url"));
        copyDivMap = copyDivMap.replace(/db_phone/g, item.get("phone"));
        copyDivMap = copyDivMap.replace(/db_site_url/g, item.get("link"));
        copyDivMap = copyDivMap.replace(/db_lat/g, item.get("lat"));
        copyDivMap = copyDivMap.replace(/db_long/g, item.get("lon"));
        copyDivMap = copyDivMap.replace(/db_category/g, item.get("category"));
        copyDivMap = copyDivMap.replace(/db_object_id/g, item.get("object_id"));

        var searchString  = "";
        
        // Decide if we search for headline or address
        if (item.get("use_headline_for_map")=="1") {
          // Encode google search URI before using
          searchString = encodeURIComponent(item.get("headline"));
        }
        else {
          // Encode google search URI before using
          searchString = encodeURIComponent(item.get("address"));
        }
        copyDiv = copyDiv.replace(/db_google_maps_search/g,searchString);
        copyDivMap = copyDivMap.replace(/db_google_maps_search/g,searchString);

        orgSearchUrl = 'https://www.google.com/maps/embed/v1/place?q=' + searchString +'&key=AIzaSyBC7pPYtgtRyu7qNcxQSk-Ld9d4gjZKz8E&language=en';
        copyDiv = copyDiv.replace(/db_google_maps_url/g,orgSearchUrl);
        copyDivMap = copyDivMap.replace(/db_google_maps_url/g,orgSearchUrl);   
        //copyDiv = copyDiv.replace("row_num", item.get("row_number"));

        // Generate offer details tag string
        if (db_details != null) {
          var detailsStr = db_details.replace(/  /g," · ");
          copyDiv = copyDiv.replace(/db_offer_details/g, detailsStr);
          copyDivMap = copyDivMap.replace(/db_offer_details/g, detailsStr);

        }
        else {
          copyDiv = copyDiv.replace(/db_offer_details/g, "");
          copyDivMap = copyDivMap.replace(/db_offer_details/g, "");
        }

        // Generate random value for random sorting
        var randValue = Math.random();
        copyDiv = copyDiv.replace(/db_random/g, String(randValue));
        copyDivMap = copyDivMap.replace(/db_random/g, String(randValue));

        //calculatedDistance = google.maps.geometry.spherical.computeDistanceBetween(
        //var userCord = new google.maps.LatLng(userLat, userLon);
        //var businessCord = new google.maps.LatLng(item.get("lat"), item.get("long"));

        //calcDistance = google.maps.geometry.spherical.computeDistanceBetween(userCord, businessCord);
        //kmCalDistance = (calcDistance / 1000).toFixed(2);
        //kmCalDistance = (10000 / 1000).toFixed(2);
        //copyDiv = copyDiv.replace(/db_distance/g, kmCalDistance);

        // Add custom div to grid
        $( ".grid" ).append(copyDiv);

        // Ready Map View
        markerUrl = "assets/" + item.get("category") + "_pin.png";
        // Add marker to map view
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(item.get("lat"), item.get("lon")),
          title: item.get("headline_en"),
          icon: markerUrl,
          map: map
        });
        // Add marker to relevant array
        category = item.get("category");
        if (category=="br") {
          br_markers.push(marker);
          }
          else if (category=="bl") {
            bl_markers.push(marker);
          }
          else if (category=="ln") {
            ln_markers.push(marker);
          }
          else if (category=="hh") {
            hh_markers.push(marker);
          }

        //bindInfoWindow(marker, map, infowindow, copyDiv);
        bindInfoWindow(marker, map, infobubble, copyDivMap);

    }
    }
}

function generateDivsJson(deal_items) {
  function bindInfoWindow(marker, map, infowindow, description) {
    marker.addListener('click', function() {
        infowindow.setContent(description);
        infowindow.open(map, this);
    });
    };

  map = new google.maps.Map(document.getElementById('map-view'), {
      zoom: 15,
      center: tlv,
      mapTypeId:google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      streetViewControl: false
    });
  var infobubble = new InfoBubble({
    disableAutoPan: false,
    maxHeight: 400,
    maxWidth: 300
    //minWidth: 320
  });

  for (var i = 0; i < deal_items.length; i++) {
      var item = deal_items[i];
      if (item.is_active=="1") {
      //  if (true) {
        var copyDiv = baseDivString;
        var copyDivMap = baseDivStringMap;
        // Check if open now
        // set not open nowclass as default
        var opening_hours = item.opening_hours;
        if (opening_hours) {
          if (isOpenNow(opening_hours)) {
            // set class to filtered one
            copyDiv = copyDiv.replace(/db_open_now/g, "open_now");
            copyDivMap = copyDivMap.replace(/db_open_now/g,"open_now");
            // display image of open_now
            copyDiv = copyDiv.replace(/db_open_image/g,"display:inline");
            copyDivMap = copyDivMap.replace(/db_open_image/g,"display:inline");
          } 
          else 
          {
            // set not open nowclass
            copyDiv = copyDiv.replace(/db_open_now/g, "not_open_now");
            copyDivMap = copyDivMap.replace(/db_open_now/g, "not_open_now");
            // display image of open_now
            copyDiv = copyDiv.replace(/db_open_image/g,"display:none");
            copyDivMap = copyDivMap.replace(/db_open_image/g,"display:none");
          };
        }
        else {
          copyDiv = copyDiv.replace(/db_open_now/g, "not_open_now");
          // display image of open_now
          copyDiv = copyDiv.replace(/db_open_image/g,"display:none");
          copyDivMap = copyDivMap.replace(/db_open_image/g,"display:none");
        };

        // Check which days its open
        if (opening_hours) {
          if (opening_hours[0] != 0 && opening_hours[0].length > 0) {
            copyDiv = copyDiv.replace(/db_open_sunday/g, "open_sunday");
          }
          if (opening_hours[1] != 0 && opening_hours[1].length > 0) {
            copyDiv = copyDiv.replace(/db_open_monday/g, "open_monday");
          }
          if (opening_hours[2] != 0 && opening_hours[2].length > 0) {
            copyDiv = copyDiv.replace(/db_open_tuesday/g, "open_tuesday");
          }
          if (opening_hours[3] != 0 && opening_hours[3].length > 0) {
            copyDiv = copyDiv.replace(/db_open_wednesday/g, "open_wednesday");
          }
          if (opening_hours[4] != 0 && opening_hours[4].length > 0) {
            copyDiv = copyDiv.replace(/db_open_thursday/g, "open_thursday");
          }
          if (opening_hours[5] != 0 && opening_hours[5].length > 0) {
            copyDiv = copyDiv.replace(/db_open_friday/g, "open_friday");
          }
          if (opening_hours.length > 6 && opening_hours[6] != 0 && opening_hours[6].length > 0) {
            copyDiv = copyDiv.replace(/db_open_saturday/g, "open_saturday");
          }
        }
        // Retrieve according to language
        if (is_english) {
          copyDiv = copyDiv.replace(/db_headline/g, item.headline_en);
          copyDiv = copyDiv.replace(/db_address/g, item.address_en);
          copyDiv = copyDiv.replace(/db_main_offer/g, item.main_offer_en);
          copyDiv = copyDiv.replace(/db_deal_hours/g, item.days_en);
          var db_details = item.offer_details_en;

          copyDivMap = copyDivMap.replace(/db_headline/g, item.headline_en);
          copyDivMap = copyDivMap.replace(/db_address/g, item.address_en);
          copyDivMap = copyDivMap.replace(/db_main_offer/g, item.main_offer_en);
          copyDivMap = copyDivMap.replace(/db_deal_hours/g, item.days_en); 

        }
        else {
          copyDiv = copyDiv.replace(/db_headline/g, item.headline);
          copyDiv = copyDiv.replace(/db_address/g, item.address);
          copyDiv = copyDiv.replace(/db_main_offer/g, item.main_offer);
          copyDiv = copyDiv.replace(/db_deal_hours/g, item.days);
          var db_details = item.offer_details;

          copyDivMap = copyDivMap.replace(/db_headline/g, item.headline);
          copyDivMap = copyDivMap.replace(/db_address/g, item.address);
          copyDivMap = copyDivMap.replace(/db_main_offer/g, item.main_offer);
          copyDivMap = copyDivMap.replace(/db_deal_hours/g, item.days);
        }

        // Items that are the same for every language
        copyDiv = copyDiv.replace(/db_image_url/g, item.image_url);
        copyDiv = copyDiv.replace(/db_icon_image_url/g, item.image_url);
        copyDiv = copyDiv.replace(/db_phone/g, item.phone);
        copyDiv = copyDiv.replace(/db_site_url/g, item.link);
        copyDiv = copyDiv.replace(/db_lat/g, item.lat);
        copyDiv = copyDiv.replace(/db_long/g, item.lon);
        copyDiv = copyDiv.replace(/db_category/g, item.category);
        copyDiv = copyDiv.replace(/db_object_id/g, item.object_id);
        
        copyDivMap = copyDivMap.replace(/db_image_url/g, item.image_url);
        copyDivMap = copyDivMap.replace(/db_icon_image_url/g, item.image_url);
        copyDivMap = copyDivMap.replace(/db_phone/g, item.phone);
        copyDivMap = copyDivMap.replace(/db_site_url/g, item.link);
        copyDivMap = copyDivMap.replace(/db_lat/g, item.lat);
        copyDivMap = copyDivMap.replace(/db_long/g, item.lon);
        copyDivMap = copyDivMap.replace(/db_category/g, item.category);
        copyDivMap = copyDivMap.replace(/db_object_id/g, item.object_id);

        var searchString  = "";
        
        // Decide if we search for headline or address
        if (item.use_headline_for_map) {
          // Encode google search URI before using
          searchString = encodeURIComponent(item.headline);
        }
        else {
          // Encode google search URI before using
          searchString = encodeURIComponent(item.address);
        }
        copyDiv = copyDiv.replace(/db_google_maps_search/g,searchString);
        copyDivMap = copyDivMap.replace(/db_google_maps_search/g,searchString);

        orgSearchUrl = 'https://www.google.com/maps/embed/v1/place?q=' + searchString +'&key=AIzaSyBC7pPYtgtRyu7qNcxQSk-Ld9d4gjZKz8E&language=en';
        copyDiv = copyDiv.replace(/db_google_maps_url/g,orgSearchUrl);
        copyDivMap = copyDivMap.replace(/db_google_maps_url/g,orgSearchUrl);   
        //copyDiv = copyDiv.replace("row_num", item.row_number"));

        // Generate offer details tag string
        if (db_details != null) {
          var detailsStr = db_details.replace(/  /g," · ");
          copyDiv = copyDiv.replace(/db_offer_details/g, detailsStr);
          copyDivMap = copyDivMap.replace(/db_offer_details/g, detailsStr);

        }
        else {
          copyDiv = copyDiv.replace(/db_offer_details/g, "");
          copyDivMap = copyDivMap.replace(/db_offer_details/g, "");
        }

        // Generate random value for random sorting
        var randValue = Math.random();
        copyDiv = copyDiv.replace(/db_random/g, String(randValue));
        copyDivMap = copyDivMap.replace(/db_random/g, String(randValue));

        //calculatedDistance = google.maps.geometry.spherical.computeDistanceBetween(
        //var userCord = new google.maps.LatLng(userLat, userLon);
        //var businessCord = new google.maps.LatLng(item.lat"), item.long"));

        //calcDistance = google.maps.geometry.spherical.computeDistanceBetween(userCord, businessCord);
        //kmCalDistance = (calcDistance / 1000).toFixed(2);
        //kmCalDistance = (10000 / 1000).toFixed(2);
        //copyDiv = copyDiv.replace(/db_distance/g, kmCalDistance);

        // Add custom div to grid
        $( ".grid" ).append(copyDiv);

        // Ready Map View
        markerUrl = "assets/" + item.category + "_pin.png";
        // Add marker to map view
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(item.lat, item.lon),
          title: item.headline_en,
          icon: markerUrl,
          map: map
        });
        // Add marker to relevant array
        category = item.category;
        if (category=="br") {
          br_markers.push(marker);
          }
          else if (category=="bl") {
            bl_markers.push(marker);
          }
          else if (category=="ln") {
            ln_markers.push(marker);
          }
          else if (category=="hh") {
            hh_markers.push(marker);
          }

        //bindInfoWindow(marker, map, infowindow, copyDiv);
        bindInfoWindow(marker, map, infobubble, copyDivMap);

    }
    }
}
function showGeneral(object_id){
  $(".item-other-info"+"."+object_id).css("display", "none");
  $(".item-other-info"+"."+object_id).removeClass("flipInY");
  $(".item-main-info"+"."+object_id).addClass('animated flipInY');
  $(".item-main-info"+"."+object_id).css("display", "block");
}

function showOther(object_id){
  $(".item-main-info"+"."+object_id).css("display", "none");
  $(".item-main-info"+"."+object_id).removeClass("animated flipInY");
  $(".item-other-info"+"."+object_id).addClass('animated flipInY');
  $(".item-other-info"+"."+object_id).css("display", "block");
  $(window).trigger("lazyupdate");
}
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    //alert("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);
    userLat = position.coords.latitude;
    userLon = position.coords.longitude;
    // Add map marker for user location
    var marker = new google.maps.Marker({
          position: new google.maps.LatLng(userLat, userLon),
          icon: 'assets/gpsloc.png',
          title: "You",
          map: map
        });
    // Make map center on user onwards
    tlv = new google.maps.LatLng(userLat, userLon);
    // Update distance in grid
    updateDistanceData();
}

function updateDistanceData() {
  var userCord = new google.maps.LatLng(userLat, userLon);
  // iterate over all deal items
  deal_items = $(".element-item");
  for (var i = deal_items.length - 1; i >= 0; i--) {
    dealLat = deal_items[i].getAttribute('lat');
    dealLong = deal_items[i].getAttribute('long');
    
    var businessCord = new google.maps.LatLng(dealLat, dealLong);
    calcDistance = google.maps.geometry.spherical.computeDistanceBetween(userCord, businessCord);
    kmCalDistance = (calcDistance / 1000).toFixed(2);
    // update attribute for sorting
    deal_items[i].setAttribute('data-distance',kmCalDistance);
    // insert data info into text
    var distanceDiv = deal_items[i].getElementsByClassName('main-distance')[0];
    distanceDiv.innerHTML = distanceDiv.innerHTML.replace(/db_distance/g, kmCalDistance);
  };
  
  // make distance text visible
  $(".main-distance").css("display", "block");
  // allow sort by distance option
  document.getElementById("sort-menu").options[4].disabled = false;
  // Update new sort Data
  $(".grid").isotope('updateSortData').isotope();

}

function switchMapFilters(itemButton, markers) {
  //check if we need to turn off or on
  if (itemButton.style.opacity==1) {
    for (i=0;i<markers.length;i++) {
      markers[i].setMap();
    }
    itemButton.setAttribute("style","opacity:0.4;");
  }
  else {
    for (i=0;i<markers.length;i++) {
      markers[i].setMap(map);
    }
    itemButton.setAttribute("style","opacity:1;");
  }
}

function gotoTopOfPage() {
  document.body.scrollTop = document.documentElement.scrollTop = 0;
}

function isOpenNow(opening_hours) {
  // get current day array
  dayArray = opening_hours[currentDay];
  // check if current time in opening frame
  if (dayArray) {
  for (i=0;i<dayArray.length;i++) {
    if (currentTimeString >= dayArray[i].start && currentTimeString <= dayArray[i].end) {
      return true;
    };
  };
  };
  return false;
}

$(document).ready(function()  {

//  $.getJSON("deal_items.json", function(json) 
if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
    xmlhttp.onreadystatechange = function() {
      if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var deal_items = jQuery.parseJSON(xmlhttp.responseText);
            for (i=0;i<deal_items.length;i++) {
              deal_items[i].opening_hours = jQuery.parseJSON(deal_items[i].opening_hours);
            }
            //var deal_items = [];
            //var firebase_active_deals = jQuery.parseJSON(xmlhttp.responseText);
            //for (var key in firebase_active_deals) {
            //  deal_items.push(firebase_active_deals[key]);
            //}
            generateDivsJson(deal_items);
            initIsotope();
            getLocation();

    //generateDivsJson(json.results);
    //initIsotope();
    //getLocation();

  // Try inserting the clost ligthbox image tag
  $('.box-close').prepend('<img class="lightbox-close-img" src="assets/close_lightbox.png"/>');

  var $grid = $('.grid');

  // Initiate smooth scrolling
  /*
  $(function() {  
    // $fn.scrollSpeed(step, speed, easing);
    jQuery.scrollSpeed(100, 800);
  });
  */    
  $(function() {
      $( '#dl-menu' ).dlmenu();
    });

      // Update category filter buttons - Desktop
    $('.site-header-button').on('click',function() {
     //$(this).toggleClass('site-header-click');
     updateFilterSelection($(this).attr('data-filter'),false);
     /* Update background of chosen category */
     $('.site-header-button').css("background-color","transparent");
     $(this).css("background-color", "rgba(168, 168, 168, 0.6)");
     });

    // Update category filter buttons - Mobile
    $('#mobile-category-menu').on('change', function() {
      updateFilterSelection($(this).val(),true);
    });

    $('#sort-menu').on('change', function() {
      // Remove animations so they don't appear post the sort/filter
      $(".item-main-info").removeClass("animated flipInY");
      $(".item-other-info").removeClass("animated flipInY");
      var sortByValue = $(this).val();
      $grid.isotope({ sortBy: sortByValue });
    });

    $('#filter-menu').on('change', function() {
     updateFilterSelection('none',true);
    });
  }};
    xmlhttp.open("GET","get_deals.php",true);
    //xmlhttp.open("GET","https://us-central1-happyhourstlv-1ecb3.cloudfunctions.net/fetchAllPlaces",true);
    xmlhttp.send();
 });

$(window).load(function() {

});

// Global vars for sorting
var currentFilterCategory="";
var currentFilterString="";
var currentFilterTime="";

// Get user location (check if its allowed)
var userLat = 0;
var userLon = 0;
